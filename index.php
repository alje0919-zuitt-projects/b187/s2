<?php require_once "./code.php" ?>


<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP SC S02 Activity</title>
</head>
<body>
	

	<h1>Divisible by 5:</h1>
	<p><?php divBy5() ?></p>

	<h1>Array Manipulation:</h1>
	
	<?php $students = [];

		array_push($students, "John Smith"); ?>

		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>

		<?php array_push($students, "Jane Smith"); ?>

		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>

		<?php array_shift($students); ?>

		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>





</body>
</html>